﻿using TMPro;
using UnityEngine;

namespace BrunoMikoski.TextJuicer.Modifiers
{
    [AddComponentMenu( "UI/Text Juicer Modifiers/Colors Gradients Modifier", 11 )]
    public sealed class TextJuicerColorGradientModifier : TextJuicerVertexModifier
    {
        [SerializeField]
        private TMP_ColorGradient[] colors;

        private Color32[] newVertexColors;

        private TMP_ColorGradient targetColor;

        public override bool ModifyGeometry
        {
            get { return false; }
        }
        public override bool ModifyVertex
        {
            get { return true; }
        }

        public override void ModifyCharacter(CharacterData characterData, TMP_Text textComponent,
            TMP_TextInfo textInfo,
            float progress,
            TMP_MeshInfo[] meshInfo)
        {
            if (colors == null || colors.Length == 0)
                return;

            int materialIndex = characterData.MaterialIndex;
            Color32[] col = textInfo.meshInfo[materialIndex].colors32;

            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            int vertexIndex = characterData.VertexIndex;

            targetColor = colors[Mathf.CeilToInt(characterData.Progress * (colors.Length-1))];
            newVertexColors[vertexIndex + 0] = targetColor.bottomLeft;
            newVertexColors[vertexIndex + 1] = targetColor.topLeft;
            newVertexColors[vertexIndex + 2] = targetColor.topRight;
            newVertexColors[vertexIndex + 3] = targetColor.bottomRight;
        }
    }
}
