﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this.gameObject);
    }
    public void CreatePlayer()
    {
        GamedataManager.Instance.playerData.Add(new PlayerData());
        InputManager.Instance.players.Add(new InputManager.InputPlayer());
    }
}
