﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEntity : MonoBehaviour
{
    [HideInInspector]
    public float zOffset;
    protected virtual void Start()
    {
        EntitiesManager.Instance.AddEntity(this);
    }
}
