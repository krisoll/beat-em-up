﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorPhysics : MonoBehaviour {
    public float xVelocity;
    public float yVelocity;
    public float jumpVelocity;
    public GameObject avatar;
    public BoxCollider2D box;
    public Rigidbody2D rigid;
    public float currentHeight;
	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.right, Input.GetAxis("Horizontal") * xVelocity * Time.deltaTime);
	}
}
