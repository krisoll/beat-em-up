﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitiesManager : MonoBehaviour {
    public static EntitiesManager Instance;
    public List<BasicEntity> ents;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        else Instance = this;
    }
    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < ents.Count; i++)
        {
            ents[i].transform.position = new Vector3(ents[i].transform.position.x,
                        ents[i].transform.position.y, ents[i].transform.position.y + ents[i].zOffset);
        }
    }
    public void AddEntity(BasicEntity b)
    {
        for(int i = 0; i < ents.Count; i++)
        {
            if (b.GetInstanceID() == ents[i].GetInstanceID()) return;
        }
        ents.Add(b);
    }
}
