﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamedataManager : MonoBehaviour
{
    public static GamedataManager Instance;
    public List<PlayerData> playerData;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
}
public class PlayerData
{
    public int id;
}