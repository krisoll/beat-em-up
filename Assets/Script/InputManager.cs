﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Rewired;
public class InputManager : MonoBehaviour {
    public static InputManager Instance;
    public int maxPlayers;
    public List<InputPlayer> players;
    // Use this for initialization
    void Awake()
    {
        Instance = this;
        InitializePlayers();
    }
    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < players.Count; i++)
        {
            players[i].Update();
        }
    }
    [System.Serializable]
    public class ActorInput
    {
        public bool stopInputs;
        public float Horizontal;
        public float Vertical;
        public Vector2 inputVelocity;
        public Action onJump = delegate { };
        public Action onAttack1 = delegate { };
        public Action onAttack2 = delegate { };
        public Action onAttack3 = delegate { };
    }
    [System.Serializable]
    public class InputPlayer : ActorInput
    {
        public Player rePlayer;
        public bool isPlaying;
        public void Start(int id)
        {
            rePlayer = ReInput.players.GetPlayer(id);
        }
        public void Update()
        {
            if (!stopInputs)
            {
                Horizontal = rePlayer.GetAxis("Horizontal");
                Vertical = rePlayer.GetAxis("Vertical");
                if (rePlayer.GetButton("Jump")) onJump();
                if (rePlayer.GetButton("Attack1")) onAttack1();
                if (rePlayer.GetButton("Attack2")) onAttack2();
                if (rePlayer.GetButton("Attack3")) onAttack3();
            }
            inputVelocity = new Vector2(Horizontal, Vertical);
        }
    }
    public void InitializePlayers()
    {
        for (int i = 0; i < maxPlayers; i++)
        {
            players.Add(new InputPlayer());
            players[i].Start(i);
        }
    }
    [System.Serializable]
    public class InputEnemy : ActorInput
    {

    }
}
