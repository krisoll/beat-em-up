﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractor : BasicEntity
{
    public Animator anim;
    public InputManager.InputPlayer plInput;
    public PlayerPhysics plPhysics;
    public Flipper flip;
    //===============Bools============
    private bool grounded = true;

    protected override void Start()
    {
        plInput = InputManager.Instance.players[0];
        base.Start();
        plInput.onJump += _Jump;
    }
    // Update is called once per frame
    void Update ()
    {
        if(!plPhysics.DontMove)CheckMove();
        CheckFlip();
	}
    void CheckMove()
    {
        if (grounded)
        {
            if (plInput.inputVelocity == Vector2.zero) anim.SetInteger("MoveState", 0);
            else anim.SetInteger("MoveState", 1);
        }
    }
    void _Jump()
    {
        StartCoroutine(Jump());
    }
    void CheckFlip()
    {
        if (plInput.inputVelocity.x < 0 && flip.flipMultiplier == 1) flip.Flip();
        else if (plInput.inputVelocity.x > 0 && flip.flipMultiplier == -1) flip.Flip();
    }

    #region [Coroutines]
    IEnumerator Jump()
    {
        grounded = false;
        anim.SetInteger("MoveState", 2);
        while (plPhysics.currentVerticalVelocity > 0)
        {
            zOffset = -transform.localPosition.y;
            yield return null;
        }
        StartCoroutine(Fall());
    }
    IEnumerator Fall()
    {
        anim.SetInteger("MoveState", 3);
        while (transform.localPosition.y > 0)
        {
            yield return null;
            zOffset = -transform.localPosition.y;
        }
        StartCoroutine(Land());
    }
    IEnumerator Land()
    {
        grounded = true;
        zOffset = 0;
        anim.SetInteger("MoveState", 4);
        yield return null;
    }
    #endregion
}
