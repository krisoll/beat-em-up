﻿using System.Collections;
using UnityEngine;

public class PlayerPhysics : MonoBehaviour {
    public PlayerInteractor pInteractor;
    public Rigidbody2D rigid;
    public Vector2 moveVelocity;
    public float jumpDelay;
    public float jumpVelocity;
    public float gravity;
    public float landDelay;
    public Vector2 currentMoveVelocity { get; private set; }
    public float currentVerticalVelocity { get; private set; }
    public bool grounded { get; private set; }
    public bool DontMove { get; private set; }
    private InputManager.InputPlayer pInput;
    // Use this for initialization
    void Start () {
        pInput = InputManager.Instance.players[0];
        pInput.onJump += Jump;
        pInput.onAttack1 += Attack1;
        grounded = true;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        CheckMove();
        CheckJump();
    }
    void CheckMove()
    {
        if (!DontMove) currentMoveVelocity = new Vector2(moveVelocity.x * pInput.Horizontal, moveVelocity.y * pInput.Vertical);
        else currentMoveVelocity = Vector2.zero;
        rigid.velocity = currentMoveVelocity;
    }
    void CheckJump()
    {
        if (!grounded)
        {
            currentVerticalVelocity -= Mathf.Sqrt(gravity * Time.deltaTime);
            pInteractor.transform.localPosition = Vector3.MoveTowards(pInteractor.transform.localPosition,
                                                    Vector3.up + pInteractor.transform.localPosition,
                                                        currentVerticalVelocity * Time.deltaTime);
            if (pInteractor.transform.localPosition.y <= 0)
            {
                StartCoroutine(LandPlayer());
            }
        }
        
    }
    private void Attack1()
    {

    }
    private void Jump()
    {
        if(grounded) StartCoroutine(JumpPlayer());
    }
    IEnumerator LandPlayer()
    {
        grounded = true;
        pInteractor.transform.localPosition = Vector3.zero;
        DontMove = true;
        yield return new WaitForSeconds(landDelay);
        DontMove = false;
    }
    IEnumerator JumpPlayer()
    {
        currentVerticalVelocity = jumpVelocity;
        DontMove = true;
        yield return new WaitForSeconds(jumpDelay);
        DontMove = false;
        grounded = false;
    }
}
